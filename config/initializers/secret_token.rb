# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b1a15e053232524ca00527361db953649e2d27e36805b61ee737b9b5b3bca7387a2ee1404559fcbd3a909440aafb26b6b35aff0e6f1c0cc7e3362fcc29ec1936'
